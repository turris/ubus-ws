#!/usr/bin/env python

#
# ubus-ws - websocket server which connects to ubus
# Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from distutils.core import setup
from ubus_ws.version import __version__

setup(
    name='ubus-ws',
    version=__version__,
    author='CZ.NIC, z.s.p.o. (http://www.nic.cz/)',
    author_email='stepan.henek@nic.cz',
    packages=['ubus_ws', ],
    scripts=['bin/ubus-ws', ],
    url='https://gitlab.labs.nic.cz/turris/ubus-ws',
    license='LICENSE.txt',
    description='Simple websocket to connect to ubus',
    long_description=open('README.txt').read(),
    requires=[
        'websocket_server'
    ],
    provides=[
        'ubus_ws'
    ],
)
