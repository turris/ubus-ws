#
# ubus-ws - websocket server which connects to ubus
# Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import websocket_server

from functools import wraps
from threading import RLock

from .log import logger


def with_lock(func):
    """Wraps function with self.lock

    :param func: the function to be wrapped
    :type func: callable
    :returns: wrapped function
    :rtype: callable
    """

    @wraps(func)
    def inner(self, *args, **kwargs):
        with self.lock:
            return func(self, *args, **kwargs)

    return inner


class Client(object):
    """ A class which represents connected client
    """

    def __init__(self, id, handler, server):
        """ Class Client init function
        :param id: client id
        :type id: int
        :param handler: client handler (will be used for sending data to client)
        :type handler: websocket_server.WebSocketHandler
        :param server: server instance
        :type server: ubus_ws.UbusWebsocketServer
        """

        self.id = id
        self.handler = handler
        self.server = server
        self.lock = RLock()
        self.threads = {}

    @with_lock
    def send_message(self, msg):
        """ sends message to the client
        :param msg: Message to be send
        :type msg: string
        """

        logger.info("Sending message to client %d: %s", self.id, msg)
        self.handler.send_message(msg)

    @with_lock
    def send_ping(self):
        """ sends ping to the client
        """

        logger.info("Sending ping to client '%d'.", self.id)
        self.handler.send_text("ping!", websocket_server.OPCODE_PING)

    @with_lock
    def add_thread(self, thread):
        """ links thread and the client instance
        :param thread: thread to be linked
        :type thread: ubus_ws.thread.BaseThread
        """
        logger.info("Adding thread '%d' to client '%d'.", thread.id, self.id)
        self.threads[thread.id] = thread

    @with_lock
    def stop_thread(self, thread_id):
        """ links thread and the client instance
        :param thread_id: id of thread to be stopped
        :type thread_id: int
        :returns: True if thread was marked to be stop False otherwise
        :rtype: bool
        """
        if thread_id in self.threads:
            logger.info("Stopping thread '%d' in client '%d'.", thread_id, self.id)
            self.threads[thread_id].mark_for_exit()
            del self.threads[thread_id]
            return True
        else:
            return False

    @with_lock
    def stop_threads(self):
        """ stops all threads
        """
        for thread_id in list(self.threads):
            self.stop_thread(thread_id)
