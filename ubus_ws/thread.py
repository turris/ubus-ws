#
# ubus-ws - websocket server which connects to ubus
# Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import os
import fcntl
import select
import subprocess
import time

from threading import Thread, Lock

from .processes import CustomPopen


class ThreadExiting(Exception):
    """ An exception to be raised when a thread is about to exit
    """

    pass


class BaseThread(Thread):
    """ Class representing the base thread
    """

    id = 0
    """ Class thread id counter """

    id_lock = Lock()
    """ Class lock for thread id counter """

    def __init__(self):
        """ the init function
        """

        super(BaseThread, self).__init__()
        self.daemon = True

        with BaseThread.id_lock:
            BaseThread.id += 1
            self.id = BaseThread.id

        self._exiting = False

    def mark_for_exit(self):
        """ set that this thread is ready for exit
        """
        self._exiting = True

    def try_exit(self):
        """ Test whether the thread is forced to exit
        :raises: ThreadExiting
        """
        if self._exiting:
            raise ThreadExiting()


class UbusThread(BaseThread):
    """ Class representing thread which is repsonsible for the communication with ubus
    """

    def __init__(self, cmd, send_function):
        """ the init function

        :param cmd: list of arguments to be used with ubus command
        :type cmd: list of str
        :param send_function: send function for sending data back to clients msg argument
        :type send_function: callable
        """
        super(UbusThread, self).__init__()
        self.cmd = ["ubus", "-S"] + cmd
        self.send_function = send_function
        self.proc = None

    def run(self):
        """ the main function of the thread """
        try:
            poller = select.epoll()
            self.proc = CustomPopen(self.cmd, stdout=subprocess.PIPE)
            stdout_fd = self.proc.stdout.fileno()

            # register for polling
            poller.register(stdout_fd, select.EPOLLHUP | select.EPOLLIN)

            # set no blocking fd
            fl = fcntl.fcntl(stdout_fd, fcntl.F_GETFL)
            fcntl.fcntl(stdout_fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

            self.buffer = bytes()

            while True:

                self.proc.poll()
                if self.proc.returncode:
                    raise ThreadExiting()
                self.try_exit()

                events = poller.poll(1)  # 1s poll
                self.try_exit()

                for fd, events in events:
                    if events & select.EPOLLHUP:
                        # program exited -> quit
                        raise ThreadExiting()
                    if events & select.EPOLLIN:
                        data = self.proc.stdout.read()
                        self.try_exit()
                        index = data.find('\n')
                        while index > 0:  # program output is split by lines
                            to_send = self.buffer + data[:index]
                            self.buffer = bytes()
                            data = data[index + 1:]
                            index = data.find('\n')
                            # TODO check json
                            if to_send:  # this will skip empty lines (\n\n)
                                self.send_function(to_send)
                            self.try_exit()

                        self.buffer += data  # append the rest into buffer

        # TODO notify client when listener dies
        except ThreadExiting:
            if self.proc:
                self.proc.kill()


class PingThread(BaseThread):
    """ Class representing thread which is repsonsible for pinging the clients
    """

    def __init__(self, ping_function):
        """ the init function
        :param ping_function: function which sends a ping to the client
        :type ping_function: callable
        """
        super(PingThread, self).__init__()
        self.ping_function = ping_function

    def run(self):
        """ the main function of the thread """
        try:
            counter = 0
            while True:
                counter += 1
                self.try_exit()
                if counter % 60 == 0:  # Ping every 60s
                    counter = 0
                    self.ping_function()
                else:
                    time.sleep(1)
                self.try_exit()

        except ThreadExiting:
            # TODO terminate the whole client when connection died
            pass
