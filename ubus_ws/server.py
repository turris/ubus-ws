#
# ubus-ws - websocket server which connects to ubus
# Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import json
import re
import socket
import subprocess
import websocket_server

from threading import Lock

from .client import Client
from .log import logger
from .processes import CustomPopen
from .thread import UbusThread, PingThread


class MalformedMessage(Exception):
    """ An exception to be raised when a message was malformed
    """
    def to_json_str(self):
        """ Returns the exception description in json string
        :returns: json string
        :rtype: str
        """
        return json.dumps(dict(error=str(self)))


class UbusWebsocketProtocol(object):
    """ A class which represents a protocol how the client and server talks

    .. note::
        the functions which starts with action_* represent the available action which
        are handled by this protocol
        the action_* signature should look like: (self, client, **kwargs)
    """

    def __init__(self, server):
        """ the init function
        :param server: the server instance
        :type server: UbusWebsocketServer
        """
        self.server = server

    @property
    def available_actions(self):
        """ list all available actions (action_*(self, client, **kwargs))
        :returns: available actions (without `action_` prefix)
        :rtype: list of str
        """

        return [e.lstrip("action_") for e in dir(self) if e.startswith("action_")]

    def parse_message(self, msg):
        """ parses the message recieved from server
        :param msg: the message (should be json string)
        :type msg: str
        :returns: (action_name, action_parameters)
        :rtype: (str, dict)
        :raises: MalformedMessage
        """

        try:
            parsed = json.loads(msg)
        except:
            raise MalformedMessage("Failed to parse client message.")
        try:
            return parsed["action"], parsed.get("params", {})
        except:
            raise MalformedMessage("Wrong msg format. Action is missing.")

    def perform_action(self, action, client, **kwargs):
        """ parses the message recieved from server
        :param action: the name of the action to be performed
        :type action: str
        :param client: Client who requested the action
        :type client: ubus_ws.Client
        :param kwargs: arguments of the action
        :type kwargs: dict
        :returns: (action_name, action_parameters)
        :rtype: (str, dict)
        :raises: MalformedMessage
        """

        if action not in self.available_actions:
            raise MalformedMessage("Unknow action '%s'." % action)
        result, params = getattr(self, "action_%s" % action)(client, **kwargs)
        return {"result": result, "params": params}

    def action_register(self, client, **kwargs):
        """ handling of the 'register' action
        The client wants to register to listen to some operations

        :param client: Client who requested the action
        :type client: ubus_ws.Client
        :param kwargs: arguments of the action should contain 'kinds' - list of events to list
        :type kwargs: dict
        :returns: ("ok", {"listener_id": <id>})  the listener_id is used for unregister
        :rtype: (str, dict)
        :raises: MalformedMessage
        """

        if 'kinds' not in kwargs:
            raise MalformedMessage("Missing 'kinds' for action register.")

        for kind in kwargs["kinds"]:
            if not re.match(r"^[a-zA-Z0-9_\-\.]*$", kind):
                raise MalformedMessage(
                    "Malformed kind (only letters, numbers and '_-.' characters are allowed).")

        client = self.server.connected_clients[client["id"]]
        # the listening is performed in another thread
        thread = UbusThread(["listen", ] + kwargs["kinds"], client.send_message)
        client.add_thread(thread)
        thread.start()

        return "ok", {"listener_id": thread.id}

    def action_unregister(self, client, **kwargs):
        """ handling of the 'unregister' action
        The client wants to unregister to listen to some operations

        :param client: Client who requested the action
        :type client: ubus_ws.Client
        :param kwargs: arguments of the action should contain 'listener_id' - id to unregister
        :type kwargs: dict
        :returns: ("ok", {"description": <text> })  text contains only some message
        :rtype: (str, dict)
        :raises: MalformedMessage
        """

        if 'listener_id' not in kwargs:
            raise MalformedMessage("Missing 'listener_id' for action unregister.")
        try:
            listener_id = int(kwargs["listener_id"])
        except ValueError:
            raise MalformedMessage("'listener_id' is not a number in action unregister.")

        client = self.server.connected_clients[client["id"]]

        # Don't kill the ping thread (id=1)
        if listener_id == 1 or not client.stop_thread(listener_id):
            raise MalformedMessage("'listener_id' is not registered.")

        return "ok", {"description": "listener terminated"}

    def prepare_response(self, response):
        """ prepare the message for the client
        :param response: response to be prepared
        :type response: dict
        :returns: response converto to json string
        :rtype: str
        """
        return json.dumps(response)


class UbusWebsocketServer(websocket_server.WebsocketServer, object):
    """ A class which represents the server
    """

    def __init__(self, *args, **kwargs):
        """ the function initialize the server instance
        """
        super(UbusWebsocketServer, self).__init__(*args, **kwargs)

        # init the lock
        self.lock = Lock()
        self.connected_clients = {}
        self.protocol = UbusWebsocketProtocol(self)

    def new_client(self, client, server):
        """ Handle when the new client was connected
        this function is used in another thread (using ThreadingMixin in the backend)

        It starts a new PingThread (client is pinged from the server)

        :param client: dict which contains "id", "handler" and "address(ip,port)"
        :type client: dict
        :param server: basically self instance
        :type server: UbusWebsocketServer
        """

        logger.info("New client '%d' connected.", client["id"])
        with self.lock:
            new_client = Client(client["id"], client["handler"], self)
            self.connected_clients[new_client.id] = new_client

        thread = PingThread(new_client.send_ping)
        new_client.add_thread(thread)
        thread.start()

    def client_left(self, client, server):
        """ Handle when the new client was connected
        properly dispose client instance and try to terminate all its threads
        this function is used in another thread (using ThreadingMixin in the backend)

        :param client: dict which contains "id", "handler" and "address(ip,port)"
        :type client: dict
        :param server: basically self instance
        :type server: UbusWebsocketServer
        """

        if client:
            logger.info("Client '%d' left.", client["id"])
            with self.lock:
                self.connected_clients[client["id"]].stop_threads()
                del self.connected_clients[client["id"]]
        else:
            logger.info("Connection closed.")

    def message_received(self, client, server, msg):
        """ Handles when a message was recieved from the client
        this function is used in another thread (using ThreadingMixin in the backend)

        :param client: dict which contains "id", "handler" and "address(ip,port)"
        :type client: dict
        :param server: basically self instance
        :type server: UbusWebsocketServer
        :param msg: the message recieved from the client
        :type msg: str
        """

        # Lock connected_client dict could be updated in the meantime
        with self.lock:
            connected_client = self.connected_clients[client["id"]]
            try:
                logger.info("Message recieved from client %d: %s", client["id"], msg)
                action, params = self.protocol.parse_message(msg)
                response = self.protocol.perform_action(action, client, **params)
                connected_client.send_message(self.protocol.prepare_response(response))
            except MalformedMessage as e:
                connected_client.send_message(e.to_json_str())

    def _pong_received_(self, handler, msg):
        """ overrides backend _pong_received_ method to print the output to console
        this function is used in another thread (using ThreadingMixin in the backend)

        :param handler: client handler (will be used for sending data to client)
        :type handler: WebSocketHandler
        :param msg: the message recieved from the client
        :type msg: str
        """

        client = self.handler_to_client(handler)
        logger.info("Recieved pong from client '%d'.", client["id"])

    def _authenticate_(self, message):
        """ overrides backend authenticate method to perform a custom authentication
        this function is used in another thread (using ThreadingMixin in the backend)

        :param message: should contain clients initial GET request
        :type message: str
        :returns: True when the authentication passes False otherwise
        :rtype: bool
        """

        cookie_lines = [
            e.strip() for e in message.split("\r\n") if e.strip().startswith("Cookie:")
        ]
        if not cookie_lines:
            return False

        ubus_ws_session_re = re.search(
            r'ubus.ws.session=([^;\s]*)', cookie_lines[0])
        if not ubus_ws_session_re:
            return False

        session_id = ubus_ws_session_re.group(1)

        # query ubus for the session
        ubus_params = {
            "ubus_rpc_session": session_id or "",
            "scope": "ubus",
            "object": "websocket-listen",
            "function": "listen-allowed"
        }

        # Verify whether the client is able to access the listen function
        args = ["ubus", "-S", "call", "session", "access", json.dumps(ubus_params)]
        proc = CustomPopen(args, stdout=subprocess.PIPE)
        proc.wait()
        if proc.returncode != 0:
            return False
        response = proc.stdout.read()
        response_parsed = json.loads(response)
        if response_parsed["access"]:
            return True
        return False


class UbusWebsocketServer6(UbusWebsocketServer):
    """ A class which represents the server (IPv6) version
    this is a subclass of UbusWebsocketServer
    """

    address_family = socket.AF_INET6
