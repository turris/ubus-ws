#
# ubus-ws - websocket server which connects to ubus
# Copyright (C) 2017 CZ.NIC, z.s.p.o. (http://www.nic.cz/)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

""" This module is designed to handle processes used by this program.

The biggest goal It is necessary to kill all subproceses when the program ends.
We use prctl function for that.

prctl is not a part of a standard library and needs to be imported via ctypes
"""

import subprocess
import prctl
import signal


class CustomPopen(subprocess.Popen):

    def __init__(self, *args, **kwargs):
        """ init function

        parameters and retvals should be the same as Popen
        except for `preexec_fn` which is overriden
        """

        # This handler is triggered after fork and before exec
        # libc.prctl sets the signal which will be send to child processes when the parent dies
        kwargs['preexec_fn'] = lambda: prctl.set_pdeathsig(signal.SIGKILL)

        super(CustomPopen, self).__init__(*args, **kwargs)
